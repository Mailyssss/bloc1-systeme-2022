# Bloc1-Systeme

***Ressources*** pour le module Bloc1 (Système)

##### 14/09/2022

- Prise de contact et création des dépôts gitlab.
- **Mission0** : Readme.md avec une descriptions de soi et l'intégration d'une photo 

##### 28/09/2022

- Début du cours sur les composants : [accessible sur gitlab](B1-Systeme_cours1-Composants.pdf)
- Lancement Mission1 : [accessible sur gitlab](B1-Systeme_Mission1.md)
- Accès en ligne de commande à Gitlab avec l'outil Gitbash sur Windows/MacOS.(Quelques commandes utiles vues en classes (bash linux) ou commandes git spécifiques)

##### 12/10/2022

- **Mission2** : [accessible sur gitlab](B1-Systeme_Mission2.md)
Travail d'équipe sur <https://gitlab.com/ESIEE-2022-2024/COMPOSANTS_PC>
	
	Ce travail, s'il est réussi, permettra à tous les présents de valoriser leur travail. **Cette production permettra de réviser pour le DS de la séance du 09/11/2022.** 

##### 26/10/2022

- **Mission2** : [accessible sur gitlab](B1-Systeme_Mission2.md)
  Travail à compléter avec pandoc.
	
- Fin du cours sur les composants : [accessible sur gitlab](B1-Systeme_cours1-Composants.pdf)

##### 09/11/2022

- **Devoir Surveillé** sur les composants d'ordinateurs, format markdown, et sur git (aucun document permis)

