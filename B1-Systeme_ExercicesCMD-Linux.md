## Exercices avec le terminal Gnu Linux
#### [Zoubeïda ABDELMOULA](mailto:zoubeida.abdelmoula@gmail.com), ESIEE-IT
#### Cours Bloc1 Système (BTS - SIO) - 09/11/2022


### Avant de commencer...

Toutes les commandes sont à consigner dans un même document :`B1_Systeme/ExercicesCMD-Linux.md`. Chaque commande est précédée par le chemin du dossier courant et accompagnée d'une explication.

On peut utiliser l'utilitaire Gitbash ou MobaXterm pour Windows pour simuler un terminal Linux.

### Exercice 1

Soit l’arborescence définie ci-dessous : 

![arborescence](img/cmd_linux_1.png)

1. Quel est le chemin absolu pour référencer le fichier `track1.mp3` ?
1. Si le dossier courant est `/home/bob`, quel est le chemin relatif vers `track1.mp3` ?
1. Si le dossier courant est `/home/bob/images`, quel est le chemin relatif vers `track1.mp3` ?
1. Si le dossier courant est `/home/bob/tmp/tests`, quel est le chemin relatif vers `track1.mp3` ?

On suppose que l’utilisateur bob est connecté et que le dossier courant est /home/bob/music.  Trouvez toutes les commandes permettant de se déplacer :
1.	Dans le dossier `rock`,
2.	Dans le dossier `tests`,
3.	Dans le dossier personnel de bob,
4.	A la racine de l’arborescence,
5.	Dans le dossier `etc`.

### Exercice 2

Votre objectif est de créer dans votre dossier personnel l’arborescence ci-dessous en n’utilisant que la ligne de commande. Les fichiers seront initialement vides.

![arborescence2](img/cmd_linux_2.png)

Pour **chaque question**, on suppose que le dossier courant de départ est votre dossier personnel. Faites la liste de toutes les commandes à exécuter dans l’ordre pour obtenir le résultat ci-dessous.

Trouvez la commande ou les commandes à exécuter pour :

1.	Afficher le chemin du dossier courant.
2.	Créer un fichier nommé `.gitignore` dans le dossier `monsite`.
3.	Afficher le contenu du dossier `monsite` avec des informations détaillées.
4.	Afficher le contenu du dossier `monsite` avec des informations détaillées et en incluant les fichiers cachés.
5.	Copier le fichier `index.html` sous le nom `index2.html`.
6.	Créer un dossier nommé `archive` dans le dossier `monsite`.
7.	Copier en une seule commande tous les fichiers HTML dans le dossier `archive`.
8.	Renommer le fichier `style.css` en `monsite.css`.
9.	Supprimer en une seule commande les fichiers `index.html` et `index2.html` présents dans `monsite`.
10.	Supprimer en une seule commande le dossier `monsite` et tout son contenu.

### Exercice 3

On suppose que le dossier courant est votre dossier personnel.
En une seule commande et sans utiliser d’éditeur de texte, trouvez comment faire pour :

- Créer un dossier `exo3`
- Vous y déplacer.
- Créer un fichier vide `moi.txt` dans le dossier courant.
- Afficher le message “Je suis en BTS SIO”.
- Ecrire le message précédent dans le fichier `moi.txt`.
- Lister le contenu (avec détail) du dossier courant. 
- Ajouter au fichier `moi.txt` le contenu proposé par le listing précédent.

En utilisant un éditeur de texte, créez le fichier `rois.txt` ayant le contenu suivant :
```
François Ier
Henri IV
Louis XIV
```
Créez aussi le fichier `rio.txt` ayant le contenu suivant :
`Rio de Janeiro est la deuxième plus grande ville du Brésil.`

En une seule commande, trouvez ensuite comment faire pour :

- Dupliquer `rois.txt` sous le nom `roisFrance.txt`.
- Déplacer tous les fichiers sauf `rio.txt` dans votre dossier personnel.
- Revenir dans votre dossier personnel.
- Supprimer les fichiers créés précédemment.
- Supprimer le dossier `exo3`.
- Réinitialiser le contenu de la console.

### Exercice 4

Trouvez comment faire pour :

- Créer le dossier `bin` dans votre dossier personnel.
- Afficher la liste des variables d’environnement.
- Afficher le nom de l’utilisateur courant.

Modifiez votre environnement pour :
- Ajouter le dossier `bin` créé précédemment à la variable `PATH`.
- Créer l’alias `ll` qui lance la commande `ls -rtlh`.

Testez vos modifications.

